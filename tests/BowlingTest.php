<?php


use App\Bowling;
use PHPUnit\Framework\TestCase;

class BowlingTest extends TestCase {
    private Bowling $game;

    protected function setUp(): void
    {
        $this->game = new Bowling();
    }

    public function testGutterGame()
    {
        $this->rollTimes(20, 0);

        $this->assertEquals(0, $this->game->score());
    }

    public function testSumsTotalFrames()
    {
        $this->rollTimes(20, 1);

        $this->assertEquals(20, $this->game->score());
    }

    public function testAddsFollowingRollForSpare()
    {
        $this->rollSpare();
        $this->rollTimes(1, 5);
        $this->rollTimes(17, 0);

        $this->assertEquals(20, $this->game->score());
    }

    public function testItAwardsATwoFrameBonusForStrike()
    {
        $this->rollTimes(1, 10);
        $this->rollTimes(1, 7);
        $this->rollTimes(1, 2);

        $this->rollTimes(17, 0);

        $this->assertEquals(28, $this->game->score());
    }

    public function testBowlsPerfectGame()
    {
        $this->rollTimes(12, 10);
        $this->assertEquals(300, $this->game->score());
    }


    private function rollSpare(): void
    {
        $this->game->roll(8);
        $this->game->roll(2);
    }

    private function rollTimes($times, $pins): void
    {
        for ($i = 0; $i < $times; $i++) {
            $this->game->roll($pins);
        }
    }
}
