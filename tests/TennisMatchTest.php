<?php

use App\TennisMatch\Player;
use App\TennisMatch\TennisMatch;
use PHPUnit\Framework\TestCase;

class TennisMatchTest extends TestCase {
    /**
     * @dataProvider scores
     */
    public function testTheScore($playerOne, $playerTwo, $score)
    {
        $p1 = new Player('player 1');
        $p2 = new Player('player 2');
        $match = new TennisMatch($p1, $p2);

        for ($i = 0; $i < $playerOne; $i++) {
            $p1->incrementScore();
        }

        for ($i = 0; $i < $playerTwo; $i++) {
            $p2->incrementScore();
        }

        $this->assertEquals($score, $match->score());
    }

    public function scores()
    {
        return [
            '0 to 0' => [0, 0, 'love-love'],
            '1 to 0' => [1, 0, 'fifteen-love'],
            '1 to 1' => [1, 1, 'fifteen-fifteen'],
            '2 to 0' => [2, 0, 'thirty-love'],
            '2 to 2' => [2, 2, 'thirty-thirty'],
            '3 to 0' => [3, 0, 'forty-love'],
            'deuce @3' => [3, 3, 'deuce'],
            'deuce @4' => [4, 4, 'deuce'],
            'advantage player 1' => [4, 3, 'Advantage: player 1'],
            'advantage player 2' => [3, 4, 'Advantage: player 2'],
            'player one wins' => [4, 0, 'Winner: Player 1'],
            'player two wins' => [2, 4, 'Winner: Player 2'],
        ];
    }
}
