<?php


use App\StringCalculator;
use PHPUnit\Framework\TestCase;
use function PHPUnit\Framework\assertSame;

class StringCalculatorTest extends TestCase {
    private StringCalculator $calculator;

    protected function setUp(): void
    {
        $this->calculator = new StringCalculator();
    }

    public function testEvalsEmptyStringAs0()
    {
        self::assertSame(0, $this->calculator->add(''));
    }

    public function testSumOfSingleNumber()
    {
        self::assertSame(5, $this->calculator->add('5'));
    }

    public function testSumOfTwoNumber()
    {
        self::assertSame(10, $this->calculator->add('5, 5'));
    }

    public function testSumOfAnyAmountOfNumber()
    {
        self::assertSame(14, $this->calculator->add('5, 5, 4'));
    }

    public function testItExceptsANewLineDelimiter()
    {
        self::assertSame(14, $this->calculator->add("5\n5\n4"));
    }

    public function testNegativeNumbersThrowException()
    {
        $this->expectException(\Exception::class);
        $this->calculator->add('5, -4');
    }

    public function testNumbersGreater1000AreIgnored()
    {
        assertSame(5, $this->calculator->add('5, 1001'));
    }

    public function testSupportsCustomDelimiters()
    {
        assertSame(9, $this->calculator->add("//:\n5:4"));
    }
}
