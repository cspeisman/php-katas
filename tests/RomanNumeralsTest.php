<?php


use App\RomanNumerals;
use PHPUnit\Framework\TestCase;

class RomanNumeralsTest extends TestCase {
    public function test1()
    {
        $this->assertEquals('I', RomanNumerals::generate(1));
    }

    public function test2()
    {
        $this->assertEquals('II', RomanNumerals::generate(2));
    }


    public function test4()
    {
        $this->assertEquals('IV', RomanNumerals::generate(4));
    }

    public function test5()
    {
        $this->assertEquals('V', RomanNumerals::generate(5));
    }

    public function test6()
    {
        $this->assertEquals('VI', RomanNumerals::generate(6));
    }

    public function test9()
    {
        $this->assertEquals('IX', RomanNumerals::generate(9));
    }

    public function test10()
    {
        $this->assertEquals('X', RomanNumerals::generate(10));
    }

    public function test11()
    {
        $this->assertEquals('XI', RomanNumerals::generate(11));
    }

    public function test20()
    {
        $this->assertEquals('XX', RomanNumerals::generate(20));
    }

    public function test40()
    {
        $this->assertEquals('XL', RomanNumerals::generate(40));
    }

    public function test50()
    {
        $this->assertEquals('L', RomanNumerals::generate(50));
    }

    public function test90()
    {
        $this->assertEquals('XC', RomanNumerals::generate(90));
    }

    public function test100()
    {
        $this->assertEquals('C', RomanNumerals::generate(100));
    }

}
