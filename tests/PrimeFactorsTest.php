<?php


use App\PrimeFactors;
use PHPUnit\Framework\TestCase;

class PrimeFactorsTest extends TestCase {
    private PrimeFactors $factors;

    protected function setUp(): void
    {
        $this->factors = new PrimeFactors;
    }

    function testPrimeFactorOf1()
    {
        $this->assertEquals([], $this->factors->generate(1));
    }

    function testPrimeFactorOf2()
    {
        $this->assertEquals([2], $this->factors->generate(2));
    }

    function testPrimeFactorOf3()
    {
        $this->assertEquals([3], $this->factors->generate(3));
    }

    function testPrimeFactorOf4()
    {
        $this->assertEquals([2, 2], $this->factors->generate(4));
    }

    function testPrimeFactorOf5()
    {
        $this->assertEquals([5], $this->factors->generate(5));
    }


    function testPrimeFactorOf6()
    {
        $this->assertEquals([2, 3], $this->factors->generate(6));
    }

    function testPrimeFactorOf8()
    {
        $this->assertEquals([2, 2, 2], $this->factors->generate(8));
    }

    function testPrimeFactorOf9()
    {
        $this->assertEquals([3, 3], $this->factors->generate(9));
    }

    function testPrimeFactorOf16()
    {
        $this->assertEquals([2, 2, 2, 2], $this->factors->generate(16));
    }

    function testPrimeFactorOf100()
    {
        $this->assertEquals([2, 2, 5, 5], $this->factors->generate(100));
    }


}
