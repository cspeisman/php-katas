<?php


namespace App;


class PrimeFactors {

    public function generate(int $num)
    {
        $primes = [];

        for ($candidate = 2; $num > 1; $candidate++) {
            while ($num % $candidate == 0) {
                $primes[] = $candidate;
                $num /= $candidate;
            }
        }

        return $primes;
    }
}