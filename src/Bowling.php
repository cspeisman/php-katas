<?php

namespace App;

class Bowling {
    const All_PINS = 10;
    private array $rolls = [];

    public function roll(int $pins)
    {
        $this->rolls[] = $pins;
    }

    public function score()
    {
        $sum = 0;
        $roll = 0;
        for ($i = 1; $i <= 10; $i++) {
            if ($this->isStrike($roll)) {
                $sum += self::All_PINS + $this->strikeBonus($roll);
                $roll += 1;
            } elseif ($this->isSpare($roll)) {
                $sum += self::All_PINS + $this->spareBonus($roll);
                $roll += 2;
            } else {
                $sum += $this->getDefaultFrameScore($roll);
                $roll += 2;
            }
        }
        return $sum;
    }

    private function isSpare(int $roll): bool
    {
        return $this->rolls[$roll] + $this->rolls[$roll + 1] == 10;
    }

    private function getDefaultFrameScore(int $roll): int
    {
        return $this->rolls[$roll] + $this->rolls[$roll + 1];
    }

    private function isStrike(int $roll): bool
    {
        return $this->rolls[$roll] == 10;
    }

    private function strikeBonus(int $roll): int
    {
        return $this->rolls[$roll + 1] + $this->rolls[$roll + 2];
    }

    private function spareBonus(int $roll): int
    {
        return $this->rolls[$roll + 2];
    }
}