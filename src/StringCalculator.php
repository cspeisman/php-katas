<?php


namespace App;


use Exception;

class StringCalculator {
    const MAX_NUMBER_ALLOWED = 1000;

    public function add(string $numbers)
    {
        $numbers = $this->parseString($numbers);

        try {
            return array_sum($this
                ->checkForNegativeNumbers($numbers)
                ->removeNumbersGreaterThanMax($numbers)
            );
        } catch (Exception $e) {
            throw $e;
        }
    }


    private function checkForNegativeNumbers(array $numbers)
    {
        foreach ($numbers as $number) {
            if ($number < 0) {
                throw new Exception('No negatives');
            }
        }
        return $this;
    }

    private function parseString(string $numbers): array
    {
        $delimiter = ",|\n";
        $delimiterRegex = "/\/\/(.)\n/";

        if (preg_match($delimiterRegex, $numbers, $matches)) {
            $delimiter = $matches[1];
            $numbers = str_replace($matches[0], '', $numbers);
        }

        $numbers = preg_split("/{$delimiter}/", $numbers);
        return $numbers;
    }


    private function removeNumbersGreaterThanMax(array $numbers)
    {
        return array_filter($numbers, function ($num) {
            return $num <= self::MAX_NUMBER_ALLOWED;
        });
    }
}