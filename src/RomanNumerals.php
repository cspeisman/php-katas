<?php


namespace App;


class RomanNumerals {
    protected static $lookup = [
        100 => 'C',
        90 => 'XC',
        50 => 'L',
        40 => 'XL',
        10 => 'X',
        9 => 'IX',
        5 => 'V',
        4 => 'IV',
        1 => 'I',
    ];

    public static function generate(int $num)
    {
        $solution = '';
        foreach (static::$lookup as $limit => $glyph) {
            while ($num >= $limit) {
                $solution .= $glyph;
                $num -= $limit;
            }
        }

        return $solution;
    }
}