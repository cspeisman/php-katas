<?php


namespace App\TennisMatch;

class TennisMatch {
    private Player $playerOne;
    private Player $playerTwo;

    public function __construct(Player $playerOne, Player $playerTwo)
    {
        $this->playerOne = $playerOne;
        $this->playerTwo = $playerTwo;
    }

    public function score()
    {
        if ($this->hasWinner()) {
            return 'Winner: Player ' . $this->leader();
        }

        if ($this->isDeuce()) {
            return 'deuce';
        }

        if ($this->hasAdvantage()) {
            return 'Advantage: player ' . $this->leader();
        }

        return sprintf(
            "%s-%s",
            $this->playerOne->getTerm(),
            $this->playerTwo->getTerm()
        );
    }

    private function hasWinner(): bool
    {
        if (max([$this->playerOne->getScore(), $this->playerTwo->getScore()]) < 4) {
            return false;
        }

        return abs($this->playerOne->getScore() - $this->playerTwo->getScore()) >= 2;
    }

    private function leader()
    {
        return $this->playerOne->getScore() > $this->playerTwo->getScore() ? '1' : '2';
    }

    private function isDeuce()
    {
        return $this->hasReachedDeuceThreshhold() && $this->playerOne->getScore() == $this->playerTwo->getScore();
    }

    private function hasAdvantage()
    {
        return $this->hasReachedDeuceThreshhold() && ! $this->isDeuce();
    }

    private function hasReachedDeuceThreshhold(): bool
    {
        return $this->playerOne->getScore() >= 3 && $this->playerTwo->getScore() >= 3;
    }
}