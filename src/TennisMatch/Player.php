<?php


namespace App\TennisMatch;


class Player {
    const TERMS = ['love', 'fifteen', 'thirty', 'forty'];
    private $name;
    /**
     * @var int
     */
    private int $score;

    /**
     * Payer constructor.
     */
    public function __construct($name)
    {
        $this->name = $name;
        $this->score = 0;
    }

    /**
     * @return int
     */
    public function getScore(): int
    {
        return $this->score;
    }

    public function getTerm()
    {
        return self::TERMS[$this->score];
    }

    public function incrementScore()
    {
        $this->score++;
    }
}